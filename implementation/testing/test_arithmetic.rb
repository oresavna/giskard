require_relative './test_main'

class TestAritExpr < TestGiskardMain
  def test_basic_operations
    assert_equal(14, interpret("11+3"))
    assert_equal(7, interpret("10-3"))
    assert_equal(0, interpret("400 + 100 - 500"))
    assert_equal(81, interpret("9*9"))
    assert_equal(21, interpret("4*5+1"))
    assert_equal(34, interpret("2*2*2*2*2 + 2"))
    assert_equal(-20, interpret("10 - 30"))
    assert_equal(-1.5, interpret("1.5 - 3.0"))
  end

  def test_associativity_parenthesis
    assert_equal(14, interpret("(10 - 3) * 2"))
    assert_equal(6, interpret("(4 - 2) * (2 + 1)"))
    assert_equal(128, interpret("2**(5+2)"))
    assert_equal(16, interpret("(2+2)**2"))
    assert_equal(18, interpret("2*(5+2*2)"))
  end

  def test_unary_minus
    assert_equal(-1, interpret("-1"))
    assert_equal(1, interpret("2-1"))
    assert_equal(-3.3, interpret("-3.3"))
    assert_equal(-3, interpret("-(1+2)"))
    assert_equal(1, interpret("-(-(-(-1)))"))
  end

  def test_powers
    #@gisk.log(true)
    assert_equal(2, interpret("2**1"))
    assert_equal(32, interpret("2**5"))
    assert_equal(34, interpret("2**5+2"))
    assert_equal(128, interpret("2**(5+2)"))
  end

  def test_floats
    assert_equal(3.3, interpret("3.3"))
    assert_equal(3.0, interpret("3.3 - 0.3"))
    assert_equal(-3.0, interpret("-3.3 + 0.3"))
    assert_equal(1.5, interpret("1.5"))
  end

  def test_nested_parenthesis
    assert_equal(40, interpret("2*(5*(2+2))"))
    assert_equal(24, interpret("2*(5*2+2)"))
    assert_equal(144, interpret("(3*(2+2))**2"))
  end
end
