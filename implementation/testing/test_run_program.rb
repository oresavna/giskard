require_relative './test_main'

class TestDeclarations < TestGiskardMain

  def test_sequence_arit_functions
    #@gisk.log(true)
    #Evaluates a file with a sequence of simple aritmetic functions.
    res = run_program("ts_gsk/tc_sequence_arit_functions.gsk")
    assert_equal(20.79, res)
  end

  def test_simple_predicates
    #@gisk.log(true)
    assert_equal("False", run_program("ts_gsk/tc_declarations_with_pred.gsk"))
  end

  def test_declarations_with_headers
    #@gisk.log(true)
    run_program("ts_gsk/tc_declarations_header.gsk")
    run_program("ts_gsk/tc_header_only_params.gsk")
  end

  def test_inline_comments
    #@gisk.log(true)
    run_program("ts_gsk/tc_inline_comments.gsk")
  end

  def test_func_call_one_arg
    #@gisk.log(true)
    assert_equal(4, run_program("ts_gsk/tc_sum_one.gsk"))
  end

  def test_func_call_two_arg
    #@gisk.log(true)
    assert_equal(8, run_program("ts_gsk/tc_sum_two_numbers.gsk"))
  end

  def test_call_as_argument
    #@gisk.log(true)
    assert_equal(15, run_program("ts_gsk/tc_call_as_arg.gsk"))
  end

  def test_max
    assert_equal(3, run_program("ts_gsk/tc_max_two_numbers.gsk"))
  end

  def test_factorial
    assert_equal(24, run_program("ts_gsk/tc_factorial.gsk"))
  end

  def test_fibonnaci
    assert_equal(5, run_program("ts_gsk/tc_fib.gsk"))
  end

  def test_many_guards
    assert_equal(8, run_program("ts_gsk/tc_fib_3guards.gsk"))
  end

  def test_multiple_functions
    assert_equal(987, run_program("ts_gsk/tc_multiple_functions.gsk"))
  end

  def test_other_functions
    assert_equal(8, run_program("ts_gsk/tc_double_times.gsk"))
  end
  def test_partial_eval
    assert_equal("1 + a", run_program("ts_gsk/tc_partial_eval.gsk"))
  end
end
