require_relative "../src/giskard"
require "test/unit"

class TestGiskardMain < Test::Unit::TestCase

  def setup
    @gisk = Giskard.new
    @gisk.log(false) # This is the parser log
  end

  def interpret(str)
    # For the interpreter, give it a main function as needed
    @gisk.parse("main="+str)
  end

  def run_program(program)
    file = File.read program
    @gisk.parse(file)
  end
end
