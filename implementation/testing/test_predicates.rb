require_relative './test_main'


class TestPredExpr < TestGiskardMain

  def test_basic_propositions
    assert_equal("False", interpret("False"))
    assert_equal("True", interpret("True"))
    assert_equal("False", interpret("False And True"))
    assert_equal("False", interpret("True And False"))
    assert_equal("False", interpret("False And False"))
    assert_equal("True", interpret("False Or True"))
    assert_equal("True", interpret("True Or False"))
    assert_equal("False", interpret("False Or False"))
    assert_equal("False", interpret("False Or False And True"))
  end

  def test_negation
    assert_equal("True", interpret("Not False"))
    assert_equal("False", interpret("Not True"))
    assert_equal("False", interpret("False Or Not True"))
    assert_equal("False", interpret("Not True And Not False"))
    assert_equal("True", interpret("Not True Or Not False"))
  end

  def test_associativity
    assert_equal("False", interpret("Not (False Or True)"))
    assert_equal("True", interpret("True And (False Or True)"))
    assert_equal("True", interpret("(True And False) Or True"))
    assert_equal("True", interpret("(False Or True) And (Not False Or True)"))
  end

  def test_nested_prepositions
    assert_equal("False", interpret("Not (Not (Not True))"))
  end
end

class TestComparisons < TestGiskardMain
  def test_simple_comparisons
    assert_equal("True", interpret("3 == 3"))
    assert_equal("True", interpret("True == True"))
    assert_equal("False", interpret("3 > 3"))
    assert_equal("False", interpret("3 < 3"))
    assert_equal("False", interpret("3 <= 0"))
    assert_equal("True", interpret("3 >= 0"))
    assert_equal("True", interpret("0 >= -1"))
    assert_equal("False", interpret("0 <= -3"))
    #Issue #27: SOLVED with 2021 version
    assert_equal("False", interpret("-3 >= 0"))
  end


  def test_complex_arit_comparisons
    assert_equal("True", interpret("(3+4) == 7"))
    assert_equal("True", interpret("40 == 2*(5*(2+2))"))
    assert_equal("True", interpret("1 == -(-(-(-1)))"))
    assert_equal("False", interpret("127 == 2**(5+2)"))
  end

  def test_complex_pred_comparisons
    assert_equal("True", interpret("True == True And (False Or True)"))
    assert_equal("True", interpret("False == (Not True)"))
    assert_equal("True", interpret("False == (Not (Not (Not True)))"))
    assert_equal("True", interpret("False == Not True"))
    assert_equal("True", interpret("False == Not (Not (Not True))"))
  end

  def test_mix_pred_arit
    assert_equal("True", interpret("3+4>5"))
    assert_equal("True", interpret("(4>3) And True"))
    assert_equal("True", interpret("(2+2>3) And True"))
  end
end
