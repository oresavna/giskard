ERROR_NO_MAIN = 'I found no main function declaration '\
     'so I don\'t know what to do with this program.'
ERROR_TYPE_NUMBER = "Wrong number of types"
ERROR_UNARY_MINUS = "Illegal expression. Unary minus on non-aritmetic expression"
ERROR_NEGATION = "Illegal expression for negation."
ERROR_INVALID = "is not a valid expression."
ERROR_GUARD_PREDICATE = "The condition cannot be evaluated to False or True"

class GskError < StandardError
end

class GskTypeError < GskError
end

class GskSyntaxError < GskError
end
