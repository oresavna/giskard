require_relative "errors"
MAIN = "main"
#=======================================================================
# CONSTRUCTS
#=======================================================================

# A program containing functions
class GskProgram
  def initialize(functions)
    @gscop = Hash.new
    functions.each {|f| @gscop[f.id] =  f }
  end

  def evaluate
    # Check if there is a main function
    main = @gscop[MAIN]
    if main.nil?
      begin
        raise GskError, ERROR_NO_MAIN
      rescue GskError => error
        puts "Error (#{error.class}): #{error.message}"
        return nil
      end
    else
      begin
        # Begin evaluation chain
        res = main.function.evaluate(@gscop)
      rescue GskError => error
        puts "Error (#{error.class}): #{error.message}"
        return nil
      end
    end
    puts "=> #{res.show}"
    res.show
  end
end

# A function with a name
class GskDeclaration
  attr_reader :id, :function
  def initialize(name, types, params, body)
    @id = name
    @function = GskFunction.new(types, params, body)
  end
  def show
    "#{id}=#{function.show}"
  end
end

# Anonymous Function
class GskFunction
  attr_reader :types, :returnType
  def initialize(types, params, body)
    @params = params
    @returnType = types.pop # Last one is returnType
    @types = types
    # Body is an GskBinary, GskCall or GskGuards
    @body = body
  end

  #Binds its params to the passed values
  def evaluate(scope, args = [])
    # Bound values make a local scope for the function
    bound = @params.zip(args).to_h
    lscop = Hash.new
    bound.map {|id, arg| lscop[id] = GskDeclaration.new(id, [], [], arg)}

    # Merges global and local scope with local priority
    scope_ = scope.merge(lscop)
    res = @body.evaluate(scope_)

    if res.is_a?(GskConstant)
      res
    else
      GskFunction.new([], [], res)
    end
  end

  def show
    if @params.empty?
      "#{@body.show}"
    else
      param_str = @params.join(" ")
      "(#{param_str}) #{@body.show}"
    end
  end
end

# Anonymous function application
class GskApplication
  def initialize(func, args)
    @func = func
    @args = args
  end
  def evaluate(scope)
    # Evaluate arguments
    args_ = @args.map { |a| a.evaluate(scope) }
    #Apply evaluated arguments on function
    @func.evaluate(scope, @args)
  end
  def show
    "#{@func.show} On (" + @args.map{ |a| a.show }.join(",")+")"
  end
end

# A named function application
class GskCall
  def initialize(id, _args = [])
    @id = id.name
    @args = _args
  end

  def evaluate(scope)
    #Retrieve function
    decl = scope[@id]

    if !decl.nil?
      func = decl.function
    else
      #if not found in scope, return self
      return self
    end

    # Evaluate arguments
    args_ = @args.map { |a| a.evaluate(scope) }

    #Type checking for arguments if given in decl
    types = func.types
    if !types.empty? then check_types(types, args_) end

    #Apply evaluated arguments on function
    res = func.evaluate(scope, args_)

    #Type checking for return value if type given in declaration
    resType = func.returnType
    if !resType.nil? then check_types([resType], [res]) end
    res
  end

  # Accessory function to check types for function application
  def check_types(types, args_)
    if (types.length != args_.length)
      raise GskTypeError, ERROR_TYPE_NUMBER
    else
      types.zip(args_).map do |t, v|
        if v.type.type != t.type
          raise GskTypeError, "#{v.value} is not #{t.type}"
        end
      end
    end
  end

  def show
    args_str = if @args.empty?
                  ""
                else
                  "("+ @args.map{ |a| a.show }.join(",")+")"
                end
    "#{@id}#{args_str}"
  end
end

# Guards are Giskard's control structures
class GskGuard
  attr_reader :condition, :return
  def initialize(comp, expr)
    @condition = comp
    @return = expr
  end

  def show()
    "Condition: #{@condition.show}, Result: #{@return.show}"
  end
end

# Gathers all guards and chooses the first one that is true
class GskGuards
  attr_reader :guards, :final
  def initialize(final, *guards)
    @guards = guards
    @final = final
  end
  def evaluate(scope)
    for guard in @guards
      res = guard.condition.evaluate(scope)
      if !res.is_a?(GskPredConstant)
        raise GskSyntaxError, ERROR_GUARD_PREDICATE + ": #{guard.condition.show}"
      else
        if res.value #If condition evaluates to true
          return guard.return.evaluate(scope)
        end
      end
    end
    #If none of the main guards was true, choose final, which is always true
    return final.return.evaluate(scope)
  end
  def show()
    "#{guards}"
    "#{final}"
  end
end

#Infix expression
class GskBinary
  def initialize(lhs, op, rhs)
    @lhs, @op, @rhs = lhs, op, rhs
  end

  def evaluate(scope)
    lhs_ = @lhs.evaluate(scope)
    rhs_ = @rhs.evaluate(scope)

    if lhs_.is_a?(GskConstant) and rhs_.is_a?(GskConstant)
      # if they're both constants, evaluate
      begin
        ruby_res = eval("#{lhs_.value} #{@op} #{rhs_.value}")
      rescue NoMethodError => error
        raise GskSyntaxError, "#{self.show} "+ERROR_INVALID
      end
    else
      # otherwise return partially evaluated expression
      return GskBinary.new(lhs_, @op, rhs_)
    end

    # Inferring type for result
    res_class = ruby_res.class
    if res_class == Integer
      GskInteger.new(ruby_res)
    elsif res_class == Float
      GskFloat.new(ruby_res)
    elsif (res_class == TrueClass || res_class == FalseClass)
      GskPredConstant.new(ruby_res)
    else
      raise GskTypeError, "Giskard has no support for #{res_class}"
    end
  end

  def show
    "#{@lhs.show} #{@op} #{@rhs.show}"
  end
end

# A fully evaluated constant -a constant function
class GskConstant
  attr_accessor :value
  def initialize(n)
    @value = n
  end
  def evaluate(scope)
    self
  end
  def show
    @value
  end
end

# Arithmetic constants like float and integer
class GskAritConstant < GskConstant
end

class GskFloat < GskAritConstant
  attr_reader :type
  def initialize(n)
    @value = n
    @type = GskType.new('float')
  end
end

class GskInteger < GskAritConstant
  attr_reader :type
  def initialize(n)
    @value = n
    @type = GskType.new('int')
  end
end

# Unary minus for aritmetic expressions
class GskUnaryMinus
  def initialize(atomic)
    @atomic = atomic
  end
  def evaluate(scope)
    #Evaluate the atomic expression and then apply the minus
    base = @atomic.evaluate(scope)
    if (base.is_a?(GskAritConstant))
      base.value *= -1
      base
    else
      raise GskTypeError, ERROR_UNARY_MINUS
    end
  end
  def show
    "- #{@atomic}"
  end
end

# Unary negation for prepositions
class GskNegation
  def initialize(pred)
    @pred = pred
  end
  #Evaluate the pred and then apply the Not
  def evaluate(scope)
    base = @pred.evaluate(scope)
    if (base.is_a?(GskPredConstant))
      base.value = eval("not #{base.value}")
      base
    else
      raise GskSyntaxError, "<#{self.show}> " + ERROR_NEGATION
    end
  end
  def show
    "Not #{@pred.show}"
  end
end

# Predicate constants (bool)
class GskPredConstant < GskConstant
  attr_reader :type
  attr_accessor :value
  def initialize(bool)
    @value = bool
    @type = GskType.new('bool')
  end
  def evaluate(scope, _args = [])
    self
  end
  def show
    if @value
      "True"
    else
      "False"
    end
  end
end

# Function names
class GskIdentifier
  attr_reader :name
  def initialize(name)
    @name = name
  end
  def show
    @name
  end
end

# Function types
class GskType
  attr_reader :type
  def initialize(type)
    @type = type
  end
  def show()
    @type
  end
end
